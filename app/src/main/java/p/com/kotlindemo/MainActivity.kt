package p.com.kotlindemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    var mName :EditText? = null
    var mPincode : EditText? = null
    var mEmail : EditText? = null
    var mMobile : EditText? = null
    var mProffession : EditText? = null
    var mCity : EditText? = null
    var mButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mName = findViewById(R.id.name_edit_text)
        mEmail = findViewById(R.id.email_edit_text);
        mPincode = findViewById(R.id.pincode_edit_text);
        mMobile = findViewById(R.id.mobile_edit_text);
        mCity = findViewById(R.id.city_edit_text);
        mProffession = findViewById(R.id.proffesion_edit_text);
        mButton = findViewById(R.id.submit_button)

        mButton?.setOnClickListener(mListener)

      //  mButton?.setOnClickListener(View.OnClickListener { v: View? ->    })

    }

    private var mListener : View.OnClickListener = View.OnClickListener{
        View -> val a : String = mMobile?.text.toString()
                Toast.makeText(this,a,Toast.LENGTH_SHORT).show()
    }



}
